import unittest
from point_of_sale import Checkout, checkout, special_offer_price
# hunk1

class TestCheckoutFunction(unittest.TestCase):
    def test_checkout(self):
        """Test if the checkout function returns the correct price for known
        cases with default special offers.
        """
        prices = {'A': 25, 'B': 40, 'P': 30}
        test_data = [  # pairs of item lists and expected totals to test
            [[], 0],
            [['A'], prices['A']],
            [['B'], prices['B']],
            [['P'], prices['P']],
            [['A', 'B', 'P'], prices['A'] + prices['B'] + prices['P']]
        ]
        for items, total in test_data:
            self.assertEqual(checkout(items, prices), total)

        # test case from problem specification
        items = ['B', 'A', 'B', 'P', 'B']
        self.assertEqual(checkout(items, prices), 155)

    def test_checkout_three_for_two_offers(self):
        """Check three_for_two_offers keyword to checkout function
        applies offers as expected"""
        prices = {'A': 25}
        # without offer
        self.assertEqual(checkout(['A'] * 3, prices,
                                  three_for_two_offers=[]), 75)
        # with offer
        self.assertEqual(checkout(['A'] * 3, prices,
                                  three_for_two_offers=['A']), 50)

    def test_checkout_three_for_price_offers(self):
        """Check three_for_price_offers keyword to checkout function
        applies offers as expected"""
        prices = {'B': 40}
        # without offer
        self.assertEqual(checkout(['B'] * 3, prices,
                                  three_for_price_offers={}), 120)
        # with offer
        self.assertEqual(checkout(['B'] * 3, prices,
                                  three_for_price_offers={'B': 100}), 100)

    def test_input_validation(self):
        """Check invalid inputs are caught as expected and valid inputs are
        not.
        """
        with self.assertRaises(ValueError):
            checkout(['A'], {}, [], {})
        checkout(['A'], {'A': 0}, [], {})

        with self.assertRaises(ValueError):
            checkout([], {'A': 0},
                     three_for_two_offers=['A'],
                     three_for_price_offers={'A': 0})

    def test_special_offer_price(self):
        """Check calculation of special offer item prices returns expected
        values.
        """
        price = 2
        offer_price = 5
        test_data = [  # test pairs of item counts and expected totals
            [0, 0], [1, 2], [2, 4], [3, 5], [4, 7], [6, 10], [7, 12]]
        for item_count, total in test_data:
            self.assertEqual(
                special_offer_price(item_count, price, offer_price), total)


class TestCheckoutClass(unittest.TestCase):
    def test_transaction(self):
        """Test a full transaction from start to finish"""
        prices = {'A': 25, 'B': 40, 'P': 30}
        items = ['B', 'A', 'B', 'P', 'B']
        co = Checkout(prices)

        for item in items:
            co.scan(item)
        self.assertEqual(co.total(), 155)

    def test_scan(self):
        """Test the scan method of the Checkout Class"""
        prices = {'A': 25, 'B': 40, 'P': 30}
        co = Checkout(prices)

        self.assertEqual(co._scanned_items, [])
        # make sure error is raised if price is unknown
        with self.assertRaises(ValueError):
            co.scan('N')
        co.scan('A')
        self.assertEqual(co._scanned_items, ['A'])

#hunk2
