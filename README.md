# ic-pos

The code for this exercise requires python >= 3.6
The only library used is unittest which is a standard within the range of required python libraries. 

The tests for this exercise can be executed with:
> python -m unittest test_point_of_sale.py
