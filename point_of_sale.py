from typing import List, Dict
# pointless comment

def checkout(items: List[str], prices: Dict[str, int],
             three_for_two_offers: List[str] = ['A'],
             three_for_price_offers: Dict[str, int] = {'B': 100}) -> int:
    """Return the total price of a list of purchased items based on the
    provided prices (in pence) and after the application of relevant
    special offers.

    Arguments
    ---------
    items:
      A list of item codes constituting a complete purchase
    prices:
      A dictionary mapping item codes to prices
    three_for_two_offers:
      A list of item codes specifying which items should have
      three for two offers applied
    three_for_price_offers:
      A dictionary where keys are item codes specifying which items
      should have a fixed price multibuy applied. The Corresponding values
      specify the offer price for triples of the item.
    """
    _validate_input(items, prices,
                    three_for_two_offers, three_for_price_offers)

    total = 0
    for item in prices:
        item_count = items.count(item)
        if item in three_for_two_offers:
            total += special_offer_price(
                item_count, prices[item], 2*prices[item])
        elif item in three_for_price_offers:
            total += special_offer_price(
                item_count, prices[item], three_for_price_offers[item])
        else:
            total += item_count * prices[item]
    return total


def _validate_input(items: List[str], prices: Dict[str, int],
                    three_for_two_offers: List[str],
                    three_for_price_offers: Dict[str, int]) -> None:
    """Check provided items and prices for obvious errors"""
    for item in items:
        if item not in prices:
            raise ValueError('Unknown price for item code %s' % item)

    for item in prices:
        if item in three_for_two_offers and item in three_for_price_offers:
            raise ValueError(
                'Multiple special offers applied to item code %s' % item)


def special_offer_price(item_count: int, price: int, offer_price: int) -> int:
    """Return the total cost for a particular item code after applying a special
    offer price to groups of three items.
    """
    multiples_of_three = item_count // 3  # no. of times to use offer price
    remainder = item_count % 3  # no. of times to use standard price
    return multiples_of_three * offer_price + remainder * price


class Checkout(object):
    """An object oriented point of sale interface. Each instance should be
    used to handle in individual transaction. Items are added to the
    transaction via the scan(item) method. The total price for all scanned
    items can be accessed via the total() method.
    """
    def __init__(self, prices: Dict[str, int],
                 three_for_two_offers: List[str] = ['A'],
                 three_for_price_offers: Dict[str, int] = {'B': 100}) -> None:
        """
        Arguments
        ---------
        prices:
          A dictionary mapping item codes to prices
        three_for_two_offers:
          A list of item codes specifying which items should have
          three for two offers applied
        three_for_price_offers:
          A dictionary where keys are item codes specifying which items
          should have a fixed price multibuy applied. The corresponding values
          specify the offer price for triples of the item.
        """
        self.prices = prices
        self.three_for_two_offers = three_for_two_offers
        self.three_for_price_offers = three_for_price_offers
        self._scanned_items: List[str] = []
        _validate_input([], self.prices,
                        self.three_for_two_offers,
                        self.three_for_price_offers)

    def scan(self, item: str) -> None:
        """Add an item to the current transaction."""
        _validate_input([item], self.prices,
                        self.three_for_two_offers,
                        self.three_for_price_offers)
        self._scanned_items.append(item)

    def total(self) -> int:
        """Return the total price of the items scanned so far."""
        return checkout(self._scanned_items, self.prices,
                        self.three_for_two_offers,
                        self.three_for_price_offers)
